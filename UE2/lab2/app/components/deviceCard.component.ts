
import {Component, Directive, HostListener, Input, ViewChild} from '@angular/core';
import {DeviceService} from "../services/device.service";
import {Device} from "../model/device";

@Component({
    selector: 'device-card',
    templateUrl: 'app/views/deviceCard.component.html'
})

export class DeviceCard {
    @Input() device: Device;

    public editState:boolean;
    public showState:boolean;

    constructor() {
        this.editState = false;
        this.showState = true;
    }


    changeEditMode(){

        this.showState = !this.showState;
        this.editState = !this.editState;
    }
}