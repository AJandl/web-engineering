import {Component, OnInit} from "@angular/core";
import {DeviceService} from "../services/device.service";

@Component({
  moduleId: module.id,
  selector: 'my-sidebar',
  templateUrl: '../views/sidebar.component.html'
})
export class SidebarComponent implements OnInit{

  failed_logins: number = 0;
  server_start: Date = new Date();

  constructor(private deviceService: DeviceService){}

  ngOnInit(): void {
    //TODO Lesen Sie über die REST-Schnittstelle den Status des Servers aus und speichern Sie diesen in obigen Variablen
    this.deviceService.readServerState().then(serverState => {
        this.failed_logins = serverState.failed_logins;
        this.server_start = serverState.server_start;
    })

  }
}
