import { Injectable }       from '@angular/core';
import { BehaviorSubject }  from "rxjs/BehaviorSubject";
import { Observable }       from "rxjs/Observable";

@Injectable()
export class NavService {

    private _showOptions: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    private _showLogout: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
    public showOptionsEmitter: Observable<boolean> = this._showOptions.asObservable();
    public showLogoutEmitter: Observable<boolean> = this._showLogout.asObservable();

    showOptions(ifShow: boolean) {
        this._showOptions.next(ifShow);
    }

    showLogout(ifShow: boolean) {
        this._showLogout.next(ifShow);
    }
}