/*jslint node: true */
/*jslint esversion: 6*/
/*jslint eqeqeq: true */

var express = require('express');
var app = express();
var fs = require("fs");
var expressWs = require('express-ws')(app);
var http = require('http');

var simulation = require('./simulation.js');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var cors = require('cors');
var uuid = require('uuid');


var oldDevices = [];
var devices = { };

var failedLogins = 0;
var serverStarted;

var wss = expressWs.getWss('/websocket');

app.set('jwtSecret', uuid.v4());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

//TODO Implementieren Sie hier Ihre REST-Schnittstelle
/* Ermöglichen Sie wie in der Angabe beschrieben folgende Funktionen:
 *  Abrufen aller Geräte als Liste
 *  Hinzufügen eines neuen Gerätes
 *  Löschen eines vorhandenen Gerätes
 *  Bearbeiten eines vorhandenen Gerätes (Verändern des Gerätezustandes und Anpassen des Anzeigenamens)
 *  Log-in und Log-out des Benutzers
 *  Ändern des Passworts
 *  Abrufen des Serverstatus (Startdatum, fehlgeschlagene Log-ins).
 *
 *  BITTE BEACHTEN!
 *      Verwenden Sie dabei passende Bezeichnungen für die einzelnen Funktionen.
 *      Achten Sie bei Ihrer Implementierung auch darauf, dass der Zugriff nur nach einem erfolgreichem Log-In erlaubt sein soll.
 *      Vergessen Sie auch nicht, dass jeder Client mit aktiver Verbindung über alle Aktionen via Websocket zu informieren ist.
 *      Bei der Anlage neuer Geräte wird eine neue ID benötigt. Verwenden Sie dafür eine uuid (https://www.npmjs.com/package/uuid, Bibliothek ist bereits eingebunden).
 */

var verify = function (req, res, next) {
    console.log("verifying...");

    jwt.verify(req.get('jwt'), app.get('jwtSecret'), function (err, token) {

        if (err) {
            console.log("verifying unsuccessful");
            console.log(err);
            res.send(false);
            return;
        }

        console.log("verifying successful");
        next();
    })
};

//Hinzufügen eines neuen Gerätes
app.post("/addDevice", verify, function (req, res) {
    "use strict";

    console.log("addDevice called");

    var device = req.body.device;
    device.id = uuid.v4();

    devices.devices.push(device);

    console.log("new device added");

    refreshConnected();
});

//Bearbeiten eines vorhandenen Gerätes (Verändern des Gerätezustandes und Anpassen des Anzeigenamens)
app.post("/updateCurrent", verify, function (req, res) {
    "use strict";
    /*
     * Damit die Daten korrekt in die Simulation übernommen werden können, verwenden Sie bitte die nachfolgende Funktion.
     *      simulation.updatedDeviceValue(device, control_unit, Number(new_value));
     * Diese Funktion verändert gleichzeitig auch den aktuellen Wert des Gerätes, Sie müssen diese daher nur mit den korrekten Werten aufrufen.
     */

    console.log("updateCurrent called");

    var id = req.body.device.id;
    for (var i = 0; i < devices.devices.length; i++) {
        if (devices.devices[i].id === id) {
            devices.devices[i].display_name = req.body.device.display_name;
            if (req.body.control_unit !== undefined && req.body.control_unit !== null) {
                for (var j=0; j < devices.device[i].control_units.length; j++){
                    if (devices.device[i].control_units[j].name === req.body.control_unit.name) {
                        devices.device[i].control_units[j].current = req.body.new_value;
                    }
                }
            }

            console.log("updated device (update)");
            break;
        }
    }

    refreshConnected();
});

//Bearbeiten eines vorhandenen Gerätes (Verändern des Gerätezustandes und Anpassen des Anzeigenamens)
app.post("/editDevice", verify, function (req, res) {
    "use strict";

    console.log("editDevice called");

    var id = req.body.device.id;
    for (var i = 0; i < devices.devices.length; i++) {
        if (devices.devices[i].id === id) {
            var control_units = req.body.device.control_units;
            for(var j = 0; j < control_units.devices.length; j++) {
                devices.devices[i].control_units[j].current = control_units[j].current;
            }
            console.log("updated device (edit)");
            break;
        }
    }

    refreshConnected();
});

//Löschen eines vorhandenen Gerätes
app.post("/deleteDevice/:deviceId", verify, function (req, res) {
    "use strict";

    var id = req.params.deviceId;

    console.log("deleteDevice called");

    for (var i = 0; i < devices.devices.length; i++) {
        if (devices.devices[i].id === id) {
            devices.devices.splice(i, 1);
            console.log("deleted device");
            break;
        }
    }

    var wrapper = { };
    wrapper.op = "delete";
    wrapper.data = id;

    wss.clients.forEach(function (client) {
        client.send(
            JSON.stringify(wrapper)
        );
    });

    refreshConnected();
});

//Abrufen aller Geräte als Liste
app.get("/getDevices", verify, function (req, res) {
    "use strict";

    console.log("getDevices called");
    oldDevices = new Array(devices.devices);

    res.send(devices.devices);
});

//Log-in des Benutzers
app.post("/login", function (req, res) {
    "use strict";

    console.log("login called");

    var username, password;
    username = req.body.username;
    password = req.body.password;

    if (username === app.get('user').username && password === app.get('user').password) {
        console.log("login valid");

        var token = jwt.sign({ username : app.get('user').username }, app.get('jwtSecret'), {
            expiresIn: '24h'
        });

        console.log("token created");

        res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
        });
    } else {
        console.log("login invalid");

        failedLogins++;
        res.status(401).send('Authentication failed');
    }
});

//Log-out des Benutzers
app.post("/logout", verify, function (req, res) {
    "use strict";

    res.status(200).send('Logout successful');

    console.log("logged out user");
});

//Benutzer Login Status
app.get("/loggedIn", verify, function (req, res) {
    "use strict";

    res.send(true);
});

//Ändern des Passworts
app.post("/changePassword", verify, function (req, res) {
    "use strict";

    var new1 = req.body.new1;
    var new2 = req.body.new2;
    var oldpw = req.body.oldpw;

    var currentUser = app.get('user');

    if (new1 !== new2) {
        res.status(400).send('new passwords do not match');
    } else if (currentUser.password !== oldpw){
        res.status(401).send('incorrect old password');
    } else {
        currentUser.password = new1;

        app.set('user', currentUser);
        var jsonfilestring = JSON.stringify(currentUser);

        fs.writeFile('resources/login.config', jsonfilestring, 'utf8', function (some) {
            res.status(200).send('password change complete');
        })

        console.log("password changed");
    }
});

//Abrufen des Serverstatus (Startdatum, fehlgeschlagene Log-ins).
app.get("/getServerState", function (req, res) {
    "use strict";

    var serverState = { failed_logins: failedLogins, server_start: serverStarted };

    console.log("Server State:");
    console.log("Failed Logins: " + serverState.failed_logins);
    console.log("Server Start: " + serverState.server_start);

    res.send(serverState);
});


function readUser() {
    "use strict";

    var file = JSON.parse(fs.readFileSync("resources/login.config"));
    app.set("user", file);
}

function readDevices() {
    "use strict";

    /*
     * Damit die Simulation korrekt funktioniert, müssen Sie diese mit nachfolgender Funktion starten
     *      simulation.simulateSmartHome(devices.devices, refreshConnected);
     * Der zweite Parameter ist dabei eine callback-Funktion, welche zum Updaten aller verbundenen Clients dienen soll.
     */

    devices = require("./resources/devices.json");
    simulation.simulateSmartHome(devices.devices, refreshConnected);
}

app.ws('/websocket', verify, function(ws, req) {
    console.log("WebSocket: Client connected");
});

function refreshConnected() {
    "use strict";

    /*
     * Jedem Client mit aktiver Verbindung zum Websocket sollen die aktuellen Daten der Geräte übermittelt werden.
     * Dabei soll jeder Client die aktuellen Werte aller Steuerungselemente von allen Geräte erhalten.
     * Stellen Sie jedoch auch sicher, dass nur Clients die eingeloggt sind entsprechende Daten erhalten.
     *
     * Bitte beachten Sie, dass diese Funktion von der Simulation genutzt wird um periodisch die simulierten Daten an alle Clients zu übertragen.
     */

    var changedDevices = [];

    for(var i = 0; i < devices.devices.length; i++) {
        var found = false;

        for(var j = 0; j < oldDevices.length; j++) {
            if(devices.devices[i].id === oldDevices[j].id) {
                found = true;

                var changedName = false;
                var changedCurrent = false;

                var temp = { };
                temp.id = devices.devices[i].id;

                if(devices.devices[i].display_name !== oldDevices[j].display_name) {
                    temp['display_name'] = devices.devices[i].display_name;

                    changedName = true;
                }

                var t = [];

                for(var a = 0; a < devices.devices[i].control_units.length; a++) {
                    t.push({
                        'current': devices.devices[i].control_units[a].current
                    });

                    if(devices.devices[i].control_units[a].current !== oldDevices[j].control_units[a].current) {
                        changedCurrent = true;
                    }
                }

                if(changedCurrent) {
                    temp['control_units'] = t;
                }

                if(changedName || changedCurrent) {
                    changedDevices.push(temp);
                }
            }
        }

        if(!found) {
            changedDevices.push(devices.devices[i]);
        }
    }

    oldDevices = JSON.parse(JSON.stringify(devices.devices));

    var wrapper = { };
    wrapper.op = "update";
    wrapper.data = changedDevices;

    console.log(changedDevices);

    wss.clients.forEach(function (client) {
        client.send(
            JSON.stringify(wrapper)
        );
    });

    console.log("Notified Clients");
}

var server = app.listen(8081, function () {
    "use strict";

    readUser();
    readDevices();
    serverStarted = new Date();
    var host = server.address().address;
    var port = server.address().port;
    console.log("Big Smart Home Server listening at http://%s:%s", host, port);
});

