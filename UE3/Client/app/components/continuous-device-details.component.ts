import {Component, Input, OnInit} from "@angular/core";
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {DeviceService} from "../services/device.service";

@Component({
  moduleId: module.id,
  selector: 'continuous-details',
  templateUrl: '../views/continuous-device-details.component.html'
})
export class ContinuousDeviceDetailsComponent implements OnInit {
  @Input()
  device: Device;

  @Input()
  controlUnit: ControlUnit;

  constructor(private deviceService: DeviceService) {
  };

  new_value: number;
  log_message: string;

  ngOnInit(): void {
    this.new_value = this.controlUnit.current;
    if (!(this.device === undefined) && !(this.device.id === undefined) && !(this.controlUnit === undefined) && !(this.controlUnit.name === undefined)) {
      let storedData: string;

      storedData = sessionStorage.getItem(this.device.id + this.controlUnit.name + "data");
      let storedLabels: string;
      storedLabels = sessionStorage.getItem(this.device.id + this.controlUnit.name + "labels");
      if (storedData !== undefined && storedData !== null && storedLabels !== undefined && storedLabels !== null) {
        this.lineChartData = Object.assign(this.lineChartData, JSON.parse(storedData));
        this.lineChartLabels = Object.assign(this.lineChartLabels, JSON.parse(storedLabels));
        this.log_message = sessionStorage.getItem(this.device.id + this.controlUnit.name + "log");
        this.controlUnit.log = this.log_message;
      }
    }
  }

  /**
   * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
   */
  onSubmit(): void {
    let time = new Date();

    let _lineChartData: Array<any> = Object.assign({}, this.lineChartData);
    _lineChartData[0].data.push(this.new_value);
    this.lineChartLabels.push(time.toLocaleDateString() + " " + time.toLocaleTimeString());
    this.lineChartData = _lineChartData;


    if (this.log_message != null) {
      this.log_message += "\n";
    } else {
      this.log_message = "";
    }
    this.log_message += new Date().toLocaleString() + ": " + this.controlUnit.current + " -> " + this.new_value;
    this.controlUnit.log = this.log_message;
    this.controlUnit.current = this.new_value;
    sessionStorage.setItem(this.device.id + this.controlUnit.name + "data", JSON.stringify(this.lineChartData));
    sessionStorage.setItem(this.device.id + this.controlUnit.name + "labels", JSON.stringify(this.lineChartLabels));
    sessionStorage.setItem(this.device.id + this.controlUnit.name + "log", this.log_message);


    this.deviceService.updateDeviceStatus(this.device, this.controlUnit);

  }

  public lineChartData: Array<any> = [
    {data: [], label: 'Verlauf'}
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false
  };
  public lineChartColors: Array<any> = [
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';
}



