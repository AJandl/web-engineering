import {Component, Input, OnInit} from '@angular/core';
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {DeviceService} from "../services/device.service";

@Component({
    moduleId: module.id,
    selector: 'boolean-details',
    templateUrl: '../views/boolean-device-details.component.html'
})
export class BooleanDeviceDetailsComponent implements OnInit {
    @Input()
    device: Device;

    @Input()
    controlUnit: ControlUnit;

    new_value: boolean;
    log_message: string = null;

    constructor(private deviceService: DeviceService) {
    }

    ngOnInit(): void {
        this.new_value = this.controlUnit.current == 1;

        if (!(this.device === undefined) && !(this.device.id === undefined) && !(this.controlUnit === undefined) && !(this.controlUnit.name === undefined)) {
            let storedData: string;

            storedData = sessionStorage.getItem(this.device.id+this.controlUnit.name);

            if (storedData !== undefined && storedData !== null) {
                this.doughnutChartData = Object.assign(this.doughnutChartData, JSON.parse(storedData));
            }
        }
    }

    /**
     * Liest den neuen Wert des Steuerungselements aus und leitet diesen an die REST-Schnittstelle weiter
     */
    onSubmit(): void {

        this.doughnutChartData[this.new_value ? 1 : 0]++;
        this.doughnutChartData = Object.assign({}, this.doughnutChartData);

        if (this.log_message != null) {
            this.log_message += "\n";
        } else {
            this.log_message = "";
        }
        this.log_message += new Date().toLocaleString() + ": " + (this.controlUnit.current == 1 ? "An" : "Aus") + " -> " + (this.new_value ? "An" : "Aus");

        this.controlUnit.log = this.log_message;
        this.controlUnit.current = this.new_value ? 1 : 0;

        sessionStorage.setItem(this.device.id+this.controlUnit.name, JSON.stringify(this.doughnutChartData));

        this.deviceService.updateDeviceStatus(this.device, this.controlUnit);
    }

    public doughnutChartData: number[] = [0, 0];
    public doughnutChartLabels: string[] = ['Aus', 'An'];
    public doughnutChartOptions: any = {
        responsive: true,
        maintainAspectRatio: false,
    };
    public doughnutChartLegend: boolean = true;
    public doughnutChartType: string = 'doughnut';

}
