import {Component, Input, OnInit} from "@angular/core";
import {ControlUnit} from "../model/controlUnit";
import {ControlType} from "../model/controlType";

@Component({
    selector: 'control',
    templateUrl: 'app/views/control.component.html'
})

export class ControlComponent implements OnInit {
    ngOnInit(): void {
        this.booleanCUString = this.controlUnit.current == 0 ? "Deaktiviert" : "Aktiviert";
    }

    @Input() controlUnit: ControlUnit;
    controlType = ControlType;

    booleannewval: number;
    contnewval: number;
    enumnewval: string;
    booleanCUString: string;
    history: Array<any> = [];
    data: string;
    labels: Array<string> = [];
    ddata: Array<number> = [];


    getValuesAsString(): void {
        this.data = "";
        for (let i = 0; i < this.history.length; i++) {
            this.data = this.data.concat(this.history[i].timestamp + " --> " + this.history[i].value + "\n");
        }
    }

    getDiagramData(): void {
        this.labels = [];
        this.ddata = [];
        switch (this.controlUnit.type) {
            case ControlType.boolean:
                this.labels.push("on");
                this.labels.push("off");
                this.ddata[0] = 0;
                this.ddata[1] = 0;
                for (let i = 0; i < this.history.length; i++) {
                    if (this.history[i].value == false) {
                        this.ddata[1] = this.ddata[1] + 1;
                    } else {
                        this.ddata[0] = this.ddata[0] + 1;
                    }
                }
                break;
            case ControlType.continuous:
                for (let i = 0; i < this.history.length; i++) {
                    this.labels.push(this.history[i].timestamp);
                    this.ddata.push(this.history[i].value);
                }
                break;
            case ControlType.enum:
                for (let i = 0; i < this.controlUnit.values.length; i++) {
                    this.labels.push(this.controlUnit.values[i]);
                    this.ddata[i] = 0;
                }
                for (let i = 0; i < this.history.length; i++) {
                    this.ddata[this.getIndexForEnumCtrlUnit(this.history[i].value)] = this.ddata[this.getIndexForEnumCtrlUnit(this.history[i].value)] + 1;
                }
                break;
        }
    }

    getIndexForEnumCtrlUnit(val: string): number {
        for (let i = 0; i < this.controlUnit.values.length; i++) {
            if (val == this.controlUnit.values[i]) {
                return i;
            }
        }
    }

    getTimestamp(): string {
        var date = new Date(Date.now() * 1000),
            datevalues = [
                date.getFullYear(),
                date.getMonth() + 1,
                date.getDate(),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds(),
            ];
        return datevalues[2] + "/" + datevalues[1] + "/" + datevalues[0] + "-" + datevalues[3] + ":" + datevalues[4] + ":" + datevalues[5];
    }

    booleanSubmit() {
        this.history.push({timestamp: this.getTimestamp(), value: this.booleannewval});
        this.booleanCUString = this.booleannewval == 0 ? "Deaktiviert" : "Aktiviert";
        this.getValuesAsString();
        this.getDiagramData();
    }

    enumSubmit() {
        this.history.push({timestamp: this.getTimestamp(), value: this.enumnewval});
        this.getValuesAsString();
        this.getDiagramData();
    }

    continousSubmit() {
        this.history.push({timestamp: this.getTimestamp(), value: this.contnewval});
        this.getValuesAsString();
        this.getDiagramData();
    }
}