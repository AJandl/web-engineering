import {Device} from '../model/device';
import {Injectable, EventEmitter} from '@angular/core';
import {Http, RequestOptions, Response, Headers} from '@angular/http'

import {DeviceParserService} from './device-parser.service';

import 'rxjs/add/operator/toPromise';
import {ServerState} from "../model/serverState";
import {ControlUnit} from "../model/controlUnit";
import {Router} from "@angular/router";


@Injectable()
export class DeviceService {

    server: string = "http://localhost:8081";
    wsServer: string = "ws://localhost:8081/websocket";
    socket: WebSocket = null;
    JWT_TOKEN: string = "jwtToken";
    devices: Device[] = [];
    started: boolean = false;
    changedEvent: EventEmitter<Device[]> = new EventEmitter<Device[]>();

    constructor(private router:Router, private parserService: DeviceParserService, private http: Http) { }

    login(username: String, password: String): Promise<any> {
        return this.http.post(this.server + "/login", { username, password })
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    loggedIn(): Promise<boolean> {
        if (localStorage.getItem(this.JWT_TOKEN) === null) {
            return Promise.resolve(false);
        }

        let headers = new Headers({
            'jwt' : localStorage.getItem(this.JWT_TOKEN)
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.get(this.server + "/loggedIn", options)
            .toPromise()
            .then(res => {
                return this.extractData(res)
                    .then(result => {
                        if(result == true) {
                            this.socket= new WebSocket(this.wsServer);
                            this.socket.onmessage = (data: MessageEvent) => {
                                var d = JSON.parse(data.data);

                                if(d.op === "delete") {
                                    this.deleteDeviceEvent(d.data);
                                } else {
                                    this.updateDevices(d.data);
                                }
                            };
                        }

                        return result;
                    });
            })
            .catch(this.handleError);
    }

    updateDeviceStatus(device:Device, controlUnit: ControlUnit) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://localhost:8081/updateCurrent');
        xhr.setRequestHeader( 'Authorization',  localStorage.getItem('jwtToken') );
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr.onload = function() {
            console.log(this.responseText);
        };
        xhr.send(JSON.stringify({device: device, control_unit: controlUnit, new_value: controlUnit.current}));

    }

    logout() {
        let headers = new Headers({
            'Content-Type' : 'application/json',
            'jwt' : localStorage.getItem(this.JWT_TOKEN)
        });

        localStorage.removeItem(this.JWT_TOKEN);

        if(this.socket !== null) {
            this.socket.close();
        }

        let options = new RequestOptions({
            headers: headers
        });

        this.http.post(this.server + "/logout", { }, options)
            .toPromise()
            .then(() => {
                this.router.navigate(['/login']);
            })
            .catch(this.handleError);
    }

    changePassword(oldPw: String, new1: String, new2: String) : Promise<any> {
        let headers = new Headers({
            'Content-Type' : 'application/json',
            'jwt' : localStorage.getItem(this.JWT_TOKEN)
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.server + "/changePassword",
            {
                new1: new1,
                new2: new2,
                oldpw: oldPw
            }
            , options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    getDevices(): Promise<Device[]> {
        if(this.started) {
            return Promise.resolve(this.devices);
        }

        this.started = true;

        let headers = new Headers({
            'jwt' : localStorage.getItem(this.JWT_TOKEN)
        });

        let options = new RequestOptions({
            headers: headers
        });

        var serverDevices =
            this.http.get(this.server + "/getDevices", options)
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);

        return Promise.resolve(serverDevices).then(devices => {
            for(var i = 0; i < devices.length; i++) {
                this.devices.push(this.parserService.parseDevice(devices[i]));
            }
            return this.devices;
        });
    }

    getDevice(id: string): Promise<Device> {
        return this.getDevices()
            .then(devices => devices.find(device => device.id === id));
    }

    deleteDeviceEvent(id: any) {
        for (let i = 0; i < this.devices.length; i++) {
            if(this.devices[i].id === id) {
                this.devices.splice(i, 1);
                break;
            }
        }

        this.changedEvent.emit(this.devices);
    }

    updateDevices(devices: any) {
        for (let i = 0; i < devices.length; i++) {
            var found = false;

            for(var j = 0; j < this.devices.length; j++) {
                if(this.devices[j].id === devices[i].id) {
                    found = true;

                    if(devices[i].hasOwnProperty('display_name')) {
                        this.devices[j].display_name = devices[i].display_name;
                    }

                    if(devices[i].hasOwnProperty('control_units')) {
                        for(var a = 0; a < devices[i].control_units.length; a++) {
                            this.devices[j].control_units[a].current = devices[i].control_units[a].current;
                        }
                    }

                    break;
                }

            }

            if(!found) {
                this.devices.push(this.parserService.parseDevice(devices[i]));
            }
        }

        this.changedEvent.emit(this.devices);
    }

    addDevice(device: Device) {
        let headers = new Headers({
            'Content-Type' : 'application/json',
            'jwt' : localStorage.getItem(this.JWT_TOKEN)});

        let options = new RequestOptions({
            headers: headers
        });

        this.http.post(this.server + "/addDevice",
            {
                device
            }
            , options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    updateCurrent(device: Device) {
        let headers = new Headers({
            'Content-Type' : 'application/json',
            'jwt': localStorage.getItem(this.JWT_TOKEN)
        });

        let options = new RequestOptions({
            headers: headers
        });

        this.http.post(this.server + "/updateCurrent/", { device }, options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    deleteDevice(device: Device) {
        let headers = new Headers({
            'Content-Type' : 'application/json',
            'jwt': localStorage.getItem(this.JWT_TOKEN)
        });

        let options = new RequestOptions({
            headers: headers
        });

        this.http.post(this.server + "/deleteDevice/" + device.id, { }, options)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    }

    readServerState(): Promise<ServerState> {
        console.log("readServerState started");

        let serverState =
            this.http.get(this.server + "/getServerState")
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);

        return Promise.resolve(serverState).then(state => {
            console.log("start state");
            console.log(state);
            console.log("end state");

            return state;
        });
    }

    private extractData(res: Response) {
        return Promise.resolve(res.json());
    }

    private handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
    }
}
