
import {Component, AfterViewInit, AfterViewChecked} from '@angular/core';
import {DeviceService} from "../services/device.service";
import {Device} from "../model/device";
import {NavService} from "../services/nav.service";

@Component({
    selector: 'overview',
    templateUrl: 'app/views/overview.component.html'
})

export class OverviewComponent implements AfterViewChecked {
    devices: Device[];

    constructor(private deviceService: DeviceService, nav: NavService) {
        nav.showOptions(true);
        nav.showLogout(true);

        deviceService.getDevices().then(val => this.devices = val);
    }

    ngAfterViewChecked(): void {
        if (this.devices) {
            this.devices.forEach(device => {
                let control_unit = device.control_units.find(unit => unit.primary)
                device.draw_image(device.id, device.image, control_unit.min, control_unit.max, control_unit.current, control_unit.values)
            })
        }
    }
}