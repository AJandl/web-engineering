/**
 * Created by Adrian on 14-Apr-17.
 */
import {Component, OnInit} from '@angular/core'
import { ActivatedRoute } from '@angular/router';
// porbably needed import {RouterModule} from '@angular/router'
import {Device} from '../model/device'
import {ControlComponent} from './control.component'
import {DeviceService} from '../services/device.service'

@Component({
    selector: 'details',
    providers: [DeviceService],
    templateUrl: 'app/views/details.component.html'
})

export class DetailComponent{
    device:Device;
    _deviceService:DeviceService;
    private sub: any;

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this._deviceService.getDevice(params['id']).then(dev => this.device=dev).catch(e => console.log(e));
        });
    }

    // TODO temporary fix, device should probably be passed later on, see below
    constructor(_deviceService:DeviceService, private route: ActivatedRoute) {
        this._deviceService = _deviceService;
    }
}