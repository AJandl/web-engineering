import {Input, OnInit} from "@angular/core";
import {Component}from '@angular/core'
/**
 * Created by Michael on 22-Apr-17.
 */

@Component({
    selector: "line-diagram",
    templateUrl: "app/views/diagrams.html"
})

export class LineDiagram implements OnInit {
    @Input() data: Array<number>;
    @Input() labels: Array<string>;

    ngOnInit(): void {
        this.chartLabels = this.labels;
        this.chartData = this.data;
    }

    public chartData: Array<any> = [];
    public chartLabels: Array<any> = [];
    public chartOptions: any = {
        responsive: true
    };

    public chartLegend: boolean = false;
    public chartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
}
