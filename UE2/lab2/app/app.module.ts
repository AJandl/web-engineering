
import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { FormsModule }          from '@angular/forms';
import { ChartsModule }         from 'ng2-charts';
import { RouterModule }         from '@angular/router'

import { AppComponent }         from './components/app.component';
import { OverviewComponent }    from "./components/overview.component";
import { HeaderComponent }      from "./components/header.component";
import { FooterComponent }      from "./components/footer.component";
import { LoginComponent }       from "./components/login.component";
import { DetailComponent }      from "./components/details.component";
import { DCDC }                 from "./components/DCDC";
import { ControlComponent }     from "./components/control.component";
import { SidebarComponent }     from "./components/sidebar.component";
import { LineDiagram }          from "./components/lineDiagram";
import { OptionsComponent }     from "./components/options.component";
import {DeviceCard} from "./components/deviceCard.component";

import { DeviceService }        from "./services/device.service";
import { NavService }           from "./services/nav.service";
import { PolarAreaChart }       from "./components/polarAreaChart.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ChartsModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full'
            }, {
                path: 'login',
                component: LoginComponent
            }, {
                path: 'options',
                component: OptionsComponent
            }, {
                path: 'overview',
                component: OverviewComponent
            }, {
                path: 'details/:id',
                component: DetailComponent
            }])
    ],
    declarations: [
        AppComponent,
        OptionsComponent,
        OverviewComponent,
        HeaderComponent,
        FooterComponent,
        LoginComponent,
        DetailComponent,
        SidebarComponent,
        DCDC,
        ControlComponent,
        LineDiagram,
        PolarAreaChart,
        DeviceCard
    ],
    providers: [
        DeviceService,
        NavService
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }