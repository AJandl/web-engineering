
import {Component} from '@angular/core'
import {NavService} from "../services/nav.service";

@Component({
    selector: 'bsh-header',
    templateUrl: 'app/views/header.component.html'
})

export class HeaderComponent {
    showOptions: boolean = false;
    showLogout: boolean = false;

    constructor(private nav: NavService) {
        this.nav.showOptionsEmitter.subscribe(mode => {
            if (mode !== null) {
                this.showOptions = mode;
            }
        });

        this.nav.showLogoutEmitter.subscribe(mode => {
            if (mode !== null) {
                this.showLogout = mode;
            }
        });
    }
}