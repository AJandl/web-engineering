/*
 TODO
 Implementieren Sie die folgenden Funktionen um die SVG-Grafiken der Geräte anzuzeigen und verändern.

 Hinweise zur Implementierung:
 - Verwenden Sie das SVG-Plugin für jQuery, welches bereits für Sie eingebunden ist (Referenz: http://keith-wood.name/svg.html)
 - Sie dürfen das Bild bei jedem Funktionenaufruf neu laden und Ihre Veränderungen vornehmen;
 Tipp: Durch Überschreiben der OnLoad Funktion von svg.load() können Sie die Grafik noch bevor diese zum ersten Mal angezeigt wird verändern
 - In allen bereit gestellten SVG-Grafiken sind alle für Sie relevanten Stellen mit Labels markiert.
 - Da Ihre Grafiken nur beim Laden der Übersichtsseite neu gezeichnet werden müssen, bietet es ich an die draw_image Funktionen nach dem vollständigen Laden dieser Seite auszuführen.
 Rufen Sie dazu mit draw_image(id, src, min, max, current, values) die zugrunde liegende und hier definierte Funktione auf.
 */


function initImage(id, src) {
    $('#' + id + " .device-alt").hide();
    return $('#' + id).svg({ loadURL: src });
}

function drawThermometer(id, src, min, max, current, values) {
    /* TODO
     Passen Sie die Höhe des Temperaturstandes entsprechend dem aktuellen Wert an.
     Beachten Sie weiters, dass auch die Beschriftung des Thermometers (max, min Temperatur) angepasst werden soll.
     */

    var svg = initImage(id, src);
    $("tspan:contains('max')", svg).html(max);
    $("tspan:contains('min')", svg).html(min);
    var path = $("#path3680", svg).attr('d');

    if (!path)
        return;

    var b4 = path.substr(0, path.indexOf('V') + 2);
    var aft = path.substr(path.indexOf('H') - 1);

    var maxV = 39;
    var minV = 323.58843;

    var diffV = minV-maxV;

    var diff = max-min;
    var curr = current-min;

    var v = ((1-curr/diff) * diffV) + maxV;

    $("#path3680", svg).attr('d', b4 + v + aft);
}


function drawBulb(id, src, min, max, current, values) {
    // TODO
    var svg = initImage(id, src);

    if (current)
        $('path', svg).attr('fill', '#ffa500');
}

function drawCam(id, src, min, max, current, values) {
    /* TODO
     Verändern Sie die Darstellung der Webcam entsprechend den Vorgaben aus der Angabe.
     Dabei soll jedoch nicht nur einfach die Farbe der Elemente verändert werden, sondern es soll eine Kopie der zu verändernden Elemente erstellt
     und anschließend die aktuellen durch die angepassten Kopien ersetzt werden.
     */
    var svg = initImage(id, src);

    if (!current) {
        var circle = $('#circle8', svg).clone();
        var shine = $('#path10', svg).clone();
        circle.removeAttr('style');
        shine.removeAttr('style');
        circle.attr('fill', '#000');
        shine.attr('fill', '#fff');
        $('#circle8', svg).replaceWith(circle);
        $('#path10', svg).replaceWith(shine);
    }
}

function drawShutter(id, src, min, max, current, values) {
    // TODO
    var svg = initImage(id, src);

    if (current <= 1) {
        $('#path4559-2-6', svg).hide();
        $('#path4559-2-5', svg).hide();
        if (current == 0) {
            $('#path4559-2', svg).hide();
        }
    }
}