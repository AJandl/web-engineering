
import {Component} from '@angular/core'
import {Router} from '@angular/router'
import {NavService} from "../services/nav.service";

@Component({
    selector: 'login',
    templateUrl: 'app/views/login.component.html'
})

export class LoginComponent {
    user: any = {};

    constructor(private router: Router, private nav: NavService) {
        nav.showOptions(false);
        nav.showLogout(false);
    }

    onSubmit() {
        if( this.user.username != "" &&
            this.user.password != "" &&
            this.user.password.length >= 4 &&
            this.user.password.length <= 12) {

            this.router.navigate(['./overview']);
        } else {
            console.log("login failed");
        }
    }
}