import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {DeviceService} from "../services/device.service";

@Component({
    moduleId: module.id,
    selector: 'my-login',
    templateUrl: '../views/login.html'
})
export class LoginComponent {

    loginError: boolean = false;

    constructor(private router: Router, private service: DeviceService) {
    }

    onSubmit(form: NgForm): void {
        if (form.valid) {
            this.service.login(form.value.username, form.value.password).then((res: any) => {
                localStorage.setItem('jwtToken', res.token);
                console.log(res);
                this.router.navigate(['/overview']);
                this.loginError = false;
            }).catch((err: any) => {
                console.error(err);
                this.loginError = true;
            });
        }
    }
}
