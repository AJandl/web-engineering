import {Input, OnInit} from "@angular/core";
import {Component}from '@angular/core'
/**
 * Created by Michael on 22-Apr-17.
 */

@Component({
    selector: "polar-area-chart",
    templateUrl: "app/views/diagrams.html"
})

export class PolarAreaChart implements OnInit {
    @Input() data: Array<number>;
    @Input() labels: Array<string>;

    ngOnInit(): void {
        this.chartLabels = this.labels;
        this.chartData = this.data;
    }
    public chartOptions:any = {
        responsive: true
    };
    public chartLabels:string[]=[];
    public chartData:number[] = [];
    public chartLegend:boolean = true;

    public chartType:string = 'polarArea';

    // events
    public chartClicked(e:any):void {
        console.log(e);
    }

    public chartHovered(e:any):void {
        console.log(e);
    }
}
