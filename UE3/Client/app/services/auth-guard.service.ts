/**
 * Created by michi on 20/05/2017.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { DeviceService } from './device.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private devService: DeviceService, private router: Router) {}

    canActivate() {
        return Promise.resolve(this.devService.loggedIn()).then(state => {
            if (state) {
                return true;
            }

            this.router.navigate(['/login']);

            return false;
        });
    }
}