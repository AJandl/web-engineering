import {Input, OnInit} from "@angular/core";
import {Component}from '@angular/core'
/**
 * Created by Adrian on 22-Apr-17.
 */

@Component({
    selector: "dcdc",
    templateUrl: "app/views/diagrams.html"
})

export class DCDC implements OnInit {
    @Input() data: Array<number>;
    @Input() labels: Array<string>;

    ngOnInit(): void {
        this.chartLabels = this.labels;
        this.chartData = this.data;
    }

    public chartLabels: string[] = [];
    public chartData: number[] = [];
    public chartType: string =
        'doughnut';

    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
}
