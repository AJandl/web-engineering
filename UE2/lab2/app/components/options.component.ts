
import {Component} from '@angular/core'
import {NavService} from "../services/nav.service";

@Component({
    selector: 'options',
    templateUrl: 'app/views/options.component.html'
})

export class OptionsComponent {
    oldPassword: string = "";
    newPassword: string = "";
    newPasswordRetype: string = "";
    isValid: boolean = false;

    constructor(private nav: NavService) {
        nav.showOptions(false);
        nav.showLogout(true);
    }

    checkPassword(): void {
        this.isValid = this.newPassword !== "" && this.newPassword === this.newPasswordRetype;
    }

    clearInputs(): void {
        this.oldPassword = "";
        this.newPassword = "";
        this.newPasswordRetype = "";
    }
}