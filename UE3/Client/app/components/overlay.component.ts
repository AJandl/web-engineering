import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {OverviewComponent} from "./overview.component";
import {DeviceService} from "../services/device.service";
import {Device} from "../model/device";
import {ControlUnit} from "../model/controlUnit";
import {ControlType} from "../model/controlType";


@Component({
    moduleId: module.id,
    selector: 'my-overlay',
    templateUrl: '../views/overlay.component.html'
})
export class OverlayComponent implements OnInit {

    @Input()
    overviewComponent: OverviewComponent = null;

    device_types: any;
    controlUnit_types: any;
    selected_type: string = null;
    controlUnitType_selected: string = null;

    addError: boolean = false;
    createError: boolean = false;

    private mapper = [
        {
            id: "Heizkörperthermostat",
            image: "images/thermometer.svg",
            image_alt: "Thermometer zur Temperatur einstellenanzeige"
        },
        {id: "Beleuchtung", image: "images/bulb.svg", image_alt: "Glühbirne als Indikator für Aktivierung"},
        {id: "Webcam", image: "images/webcam.svg", image_alt: "Webcam als Indikator für Aktivierung"},
        {
            id: "Überwachungskamera",
            image: "images/webcam.svg",
            image_alt: "Webcam als Indikator für Aktivierung"
        },
        {
            id: "Rollladen",
            image: "images/roller_shutter.svg",
            image_alt: "Rollladenbild als Indikator für Öffnungszustand"
        }];

    constructor(private deviceService: DeviceService) {
    }


    ngOnInit(): void {
        this.device_types = ["Beleuchtung", "Heizkörperthermostat", "Rollladen", "Überwachungskamera", "Webcam"];
        this.controlUnit_types = ["Ein/Auschalter", "Diskrete Werte", "Kontinuierlicher Wert"];
        this.selected_type = this.device_types[0];
        this.controlUnitType_selected = this.controlUnit_types[0];
    }

    doClose(): void {
        if (this.overviewComponent != null) {
            this.overviewComponent.closeAddDeviceWindow();
        }
    }

    /**
     * Liest die Daten des neuen Gerätes aus der Form aus und leitet diese an die REST-Schnittstelle weiter
     * @param form
     */
    onSubmit(form: NgForm): void {

        this.overviewComponent.closeAddDeviceWindow();

        //form.value.type-input; TODO FIXME?
        //form.value.elementtype-input; TODO FIXME?
        console.log(">>>>>" + form.value.displayname, "", form.value.typename, form.value.elementname, "");
        let device = new Device();
        device.description = "Beschreibung";
        device.display_name = form.value.displayname;
        device.type = form.value["type-input"];
        device.type_name = form.value.typename;
        for (let i = 0; i < this.mapper.length; i++) {
            if (this.mapper[i].id === device.type) {
                device.image = this.mapper[i].image;
                device.image_alt = this.mapper[i].image_alt;
            }
        }
        let controlUnit = new ControlUnit();
        controlUnit.name = this.controlUnitType_selected;
        if (this.isBooleanSelected()) {
            controlUnit.type = ControlType.boolean;
        } else if (this.isContinuousSelected()) {
            controlUnit.type = ControlType.continuous;
            controlUnit.min = form.value["minimum-value"];
            controlUnit.max = form.value["maximum-value"];
        } else {
            controlUnit.type = ControlType.enum;
            controlUnit.values = form.value["discrete-values"];
        }
        controlUnit.primary = true;
        device.control_units = [controlUnit];
        this.deviceService.addDevice(device);
        form.reset();
    }

    isSelected(type: string): boolean {
        return type == this.device_types[0];
    }

    isBooleanSelected(): boolean {
        return this.controlUnitType_selected === this.controlUnit_types[0];
    }

    isEnumSelected(): boolean {
        return this.controlUnitType_selected === this.controlUnit_types[1];
    }

    isContinuousSelected(): boolean {
        return this.controlUnitType_selected === this.controlUnit_types[2];
    }

}
